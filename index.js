// Setup express server
var express = require('express');
var app = express();
var path = require('path');
var http = require('http').Server(app);
var io = require('socket.io')(http);

http.listen(3000, () => {
  console.log('listening on port: 3000');
});

// Routes
app.use(express.static(path.join(__dirname, 'public')));

var onlineUsers = [];
var history = [];
io.on('connection', socket => {
  socket.on('chat message', (nickname, message) => {
    var user = { nickname, id: socket.id };
    history.push({ user, message, type: 'message' });
    io.emit('chat message', user, message);
  });

  socket.on('user disconnect', nickname => {
    var user = { nickname, id: socket.id };
    onlineUsers = onlineUsers.filter(obj => obj.id != socket.id);
    history.push({ user, message: 'left', type: 'connection' });
    io.emit('user disconnect', user, onlineUsers);
  });

  socket.on('user connect', nickname => {
    var user = { nickname, id: socket.id };
    onlineUsers.push(user);
    history.push({ user, message: 'joined', type: 'connection' });
    io.emit('user connect', user, onlineUsers, history);
  });
});
