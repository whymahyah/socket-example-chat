### Socket Chat App ###

A simple chat app built with socket.io, express and node.

### How to get started ###

```
$ git clone git@bitbucket.org:whymahyah/socket-example-chat.git
$ cd socket-example-chat/
$ npm install
$ node index.js
```

Open browser and point it to `http://localhost:3000`.

### Feature ###

1. Users can chat using their entered username
2. Has list of online users
3. Users are notified when a user joins/leaves
