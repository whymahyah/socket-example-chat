$(() => {
  var socket = io();
  do {
    var nickname = prompt('Please enter nickname');
  } while (nickname == null || nickname == '');

  // Notify server when a user connects
  socket.emit('user connect', nickname);

  // Send message to the server
  $('form').submit(function() {
    socket.emit('chat message', nickname, $('input').val());
    $('input').val('');
    return false;
  });

  // Update messages list
  socket.on('chat message', (user, message) => {
    $('#messages').append(
      $('<li class="list-group-item">').text(user.nickname + ': ' + message)
    );
  });

  // Update when a user connects
  socket.on('user connect', (user, onlineUsers, history) => {
    $('#messages').empty();
    $('#users').empty();

    // Add past messages into the list of messages
    history.forEach(element => {
      if (element.type === 'message') {
        $('#messages').append(
          $('<li class="list-group-item text-center">').text(
            element.user.nickname + ': ' + element.message
          )
        );
      }

      if (element.type === 'connection') {
        $('#messages').append(
          $('<li class="list-group-item">').append(
            $('<span class="font-weight-light badge badge-success">').text(
              element.user.nickname + ' ' + element.message
            )
          )
        );
      }
    });

    // Update list of Online Users
    onlineUsers.forEach(element => {
      if (socket.id === element.id)
        $('#users').append(
          $('<li class="list-group-item active">').text(element.nickname)
        );
      else
        $('#users').append(
          $('<li class="list-group-item">').text(element.nickname)
        );
    });
  });
  // Update when a user disconnects
  socket.on('user disconnect', (user, onlineUsers) => {
    $('#messages').append(
      $('<li class="list-group-item">').append(
        $('<span class="font-weight-light badge badge-danger">').text(
          user.nickname + ' disconnected.'
        )
      )
    );
    $('#users').empty();

    // Update list of Online Users
    onlineUsers.forEach(element => {
      $('#users').append(
        $('<li class="list-group-item">').text(element.nickname)
      );
    });
  });

  // Notify server if user closes his window
  $(window).unload(function() {
    socket.emit('user disconnect', nickname);
  });
});
